package business_Logic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import model.orders;

public class ordersOperation {
	public void insert(Connection con, orders order,String nume_client, String articol,int nr_buc) throws SQLException {
		String statement = "INSERT INTO orders ( Nume_Client,produs,nr_buc ) VALUES (?,?,?)";
		PreparedStatement prepSt = con.prepareStatement(statement);
		
		order.set_Nume_Client(nume_client);
		order.set_produs(articol);
		order.set_nr_buc(nr_buc);
		prepSt.setString(1, order.get_Nume_Client());
		prepSt.setString(2, order.get_produs());
		prepSt.setInt(3,order.get_nr_buc());
		prepSt.executeUpdate();
	}
	public void delete(Connection con, orders order,String produs) throws SQLException {
		String statement = "DELETE FROM orders where Order_ID=?";
		PreparedStatement prepSt = con.prepareStatement(statement);
		order.set_produs(produs);
		prepSt.setInt(1, order.get_Order_ID());
		prepSt.executeUpdate();
	}
	
	
	public  String raport(Connection connection ) throws SQLException {
	System.out.println("Creating Statement...");
	Statement stmt = connection.createStatement();
	String sqlQuery = "SELECT * FROM orders";
	ResultSet queryResult = stmt.executeQuery(sqlQuery);
	String S;
	StringBuilder builder = new StringBuilder();// extract data from result set
	while (queryResult.next()) {
		// retrieve by column name
		int id_order = queryResult.getInt("Order_ID");
		String nume_client = queryResult.getString("Nume_Client");
		String prod_name = queryResult.getString("produs");
		int nr_buc = queryResult.getInt("nr_buc");
        builder.append("Order_ID");
        builder.append(nume_client);
        builder.append(", id_client:");
        builder.append(id_order);
        builder.append(",Product name:");
        builder.append(prod_name);
        builder.append(",Nr_buc");
        builder.append(nr_buc);
        
		S=builder.toString();
		
	 }
	queryResult.close();
	stmt.close();
	S=builder.toString();
	  return S;
	}
	
	
	
	/*public void update(Connection con, orders order, int Order_ID, String produs, int nr_buc )
			throws SQLException {
		String statement = "UPDATE student SET  produs=?, nr_buc=? where studentID=?";
		PreparedStatement prepSt = con.prepareStatement(statement);
		prepSt.setString(1, produs);
		prepSt.setInt(2, nr_buc);
		prepSt.executeUpdate();
  }*/
}
