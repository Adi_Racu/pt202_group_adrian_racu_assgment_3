
package business_Logic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import model.clienti;

public class clientiOperations {
	public void insert(Connection con, clienti client,String nume,String adress) throws SQLException {
		String statement = "INSERT INTO clienti (  Nume_Client,Adresa ) VALUES (?,?)";
		PreparedStatement prepSt = con.prepareStatement(statement);
		client.set_nume_Clients(nume);
		client.set_Adresa_Clients(adress);
		prepSt.setString(1, client.getNume_Client());
		prepSt.setString(2, client.getAdress_Client());
		prepSt.executeUpdate();
	}

	public void delete(Connection con, clienti client,String nume) throws SQLException {
		String statement = "DELETE FROM clienti where Nume_Client=?";
		PreparedStatement prepSt = con.prepareStatement(statement);
		/*client.set_nume_Clients(nume);
		prepSt.setString(1, client.getNume_Client());*/
		prepSt.setString(1, nume);
		prepSt.executeUpdate();
		
	}
	public  String raport(Connection connection ) throws SQLException {
		System.out.println("Creating Statement...");
		Statement stmt = connection.createStatement();
		String sqlQuery = "SELECT * FROM clienti";
		ResultSet queryResult = stmt.executeQuery(sqlQuery);
		String S;
		StringBuilder builder = new StringBuilder();// extract data from result set
		while (queryResult.next()) {
			// retrieve by column name
			int id = queryResult.getInt("id_client");
			String name = queryResult.getString("Nume_Client");
			String adress = queryResult.getString("Adresa");
            builder.append("ID:");
            builder.append(id);
            builder.append(", Nume:");
            builder.append(name);
            builder.append(",Adresa:");
            builder.append(adress);
			S=builder.toString();
			
		 }
		queryResult.close();
		stmt.close();
		S=builder.toString();
		  return S;
		}
	/*public void update(Connection con, clienti client, int id_clientPrimary, String Nume_Client)
			throws SQLException {
		String statement = "UPDATE student SET  Nume_Client=? where studentID=?";
		PreparedStatement prepSt = con.prepareStatement(statement);
		prepSt.setString(1, Nume_Client);
		prepSt.executeUpdate();
  }*/
}
