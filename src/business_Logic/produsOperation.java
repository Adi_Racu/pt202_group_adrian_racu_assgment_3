package business_Logic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import model.produs;

public class produsOperation {
	public void insert(Connection con, produs produs,String nume,int nr) throws SQLException {
		String statement = "INSERT INTO products ( Prod_name,Nr_prod ) VALUES (?,?)";
		PreparedStatement prepSt = con.prepareStatement(statement);
		produs.set_Prod_name(nume);
		produs.set_Nr_prod(nr);
		prepSt.setString(1, produs.get_Prod_name());
		prepSt.setInt(2, produs.get_Nr_prod());

		prepSt.executeUpdate();
	}
	public  String raport(Connection connection ) throws SQLException {
		System.out.println("Creating Statement...");
		Statement stmt = connection.createStatement();
		String sqlQuery = "SELECT * FROM products";
		ResultSet queryResult = stmt.executeQuery(sqlQuery);
		String S;
		StringBuilder builder = new StringBuilder();// extract data from result set
		while (queryResult.next()) {
			// retrieve by column name
			int id_prod = queryResult.getInt("Product_ID");
			String prod_name = queryResult.getString("Prod_name");
			int nr_prod_nr = queryResult.getInt("Nr_prod");
            builder.append("ID_Produs");
            builder.append(id_prod);
            builder.append(",Product name:");
            builder.append(prod_name);
            builder.append(",Nr_prod");
            builder.append(nr_prod_nr);
            
			S=builder.toString();
			
		 }
		queryResult.close();
		stmt.close();
		S=builder.toString();
		  return S;
		}
	
	
	
	
	public void delete(Connection con, produs produs,String nume) throws SQLException {
		String statement = "DELETE FROM products where Prod_name=?";
		PreparedStatement prepSt = con.prepareStatement(statement);
		produs.set_Prod_name(nume);
		prepSt.setString(1, produs.get_Prod_name());
		prepSt.executeUpdate();
	}
}
