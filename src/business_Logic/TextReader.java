package business_Logic;

import java.io.IOException;
import java.sql.SQLException;

import dataAcessLayer.DBConnection;
import model.clienti;
import model.orders;
import model.produs;

public class TextReader {
	String command;
	String s0,s1,s2;
	public TextReader() {
		
	}
	public TextReader(String command) {
		this.command=command;
	}
	public void action() {
		DBConnection conexiune = DBConnection.getConnection();	
		clientiOperations clientop=new clientiOperations();
		ordersOperation orderop=new ordersOperation();
		produsOperation prodop=new produsOperation();
		textParser pars=new textParser();
			String[] com=command.split(":");
			String op=com[0];
			System.out.println(op);
			switch(op) {
			case"Insert client":
			String info=com[1];
			System.out.println(info);
			String[] com1=info.split(",");
			String num=com1[0];
			System.out.println(num);
			String adress=com1[1];
			try {
			clienti client=new clienti();
			clientop.insert(conexiune.connection,client,num,adress);}                 
			 catch (SQLException e) {
			  		e.printStackTrace();}
			      break;  
			case"Delete client":
			String num_del=com[1];
			try {
			clienti client=new clienti();
			clientop.delete(conexiune.connection, client,num_del );}
			catch (SQLException e) {
		  		e.printStackTrace();}
		      break;  
			case"Insert product":
				String info2=com[1];
				String[] com2=info2.split(",");
				String nume=com2[0];
				int pret=Integer.parseInt(com2[1]);
				try {
				produs prod=new produs();
				prodop.insert(conexiune.connection,prod,nume,pret);}                 
				 catch (SQLException e) {
				  		e.printStackTrace();}
				      break;    
			case"Delete product":
				String prod_del=com[1];
				try {
				produs prod=new produs();
				prodop.delete(conexiune.connection, prod,prod_del);}
				catch (SQLException e) {
			  		e.printStackTrace();}
			      break;  
			case"Order":
				String info3=com[1];
				String[] com3=info3.split(",");
				String nume1=com3[0];
				String articol=com3[1];
				int nr=Integer.parseInt(com3[2].trim());
				try {
				orders prod=new orders();
				orderop.insert(conexiune.connection,prod,nume1,articol,nr);}                 
				 catch (SQLException e) {
				  		e.printStackTrace();}
				      break; 
			case"Report":
				String info4=com[1];
				if(info4.equals("client")) {
					try {
						String raport=clientop.raport(conexiune.connection);
								pars.textCreate(raport);}                 
						 catch (SQLException e) {
						  		e.printStackTrace();} catch (IOException e) {
							
							e.printStackTrace();
						}
						      break; 	  	
				}
				else if(info4.equals("order")) {
					try {
						String raport=prodop.raport(conexiune.connection);
						pars.textCreate(raport)	;}                 
						 catch (SQLException e) {
						  		e.printStackTrace();} catch (IOException e) {
							e.printStackTrace();
						}
						      break; 	  			
				   }
				else if(info4.equals("product")) {
					try {
						String raport=orderop.raport(conexiune.connection);
						pars.textCreate(raport);}                 
						 catch (SQLException e) {
						  		e.printStackTrace();} catch (IOException e) {
			
							e.printStackTrace();
						}
						      break; 	  			
				   }      
			default:
				System.out.println("NU RETURNEAZA");
				break;
			}
			
		}	
}
